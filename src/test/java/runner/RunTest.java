package runner;

//import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import io.cucumber.testng.CucumberOptions;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import org.junit.runner.RunWith;

//@RunWith(Cucumber.class)
@CucumberOptions(features = {"src/test/resources/features/Customer.feature"},
                plugin ={"json:target/cucumber-reports/CucumberTestReport.json",
                "html:target/html/native-cucumber-reporting.html"},
                dryRun = false,
                glue = "stepDefine",
                tags = "@regression")
public class RunTest extends AbstractTestNGCucumberTests {
}
