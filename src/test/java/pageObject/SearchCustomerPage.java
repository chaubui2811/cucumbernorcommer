package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import utilities.WaitHelper;

import java.lang.reflect.Array;
import java.util.List;

public class SearchCustomerPage extends  BasePOM{
    WaitHelper waithelper;
    public SearchCustomerPage(WebDriver driver) {
        super(driver);
        WaitHelper waitHelper = new WaitHelper(driver);
    }

    //Locate elements
    @FindBy(id = "SearchEmail")
    @CacheLookup
    WebElement searchEmail;

    @FindBy(id = "SearchFirstName")
    @CacheLookup
    WebElement searchFirstName;

    @FindBy(id = "SearchLastName")
    @CacheLookup
    WebElement searchLastName;

    @FindBy(id = "search-customers")
    @CacheLookup
    WebElement btnSearchCustomers;

    @FindBy(how = How.XPATH, using = "//table[@id='customers-grid']//tbody/tr")
    List<WebElement> tableRows;

    @FindBy(xpath = "//table[@id='customers-grid']")
    @CacheLookup
     WebElement table;

   // List<WebElement> listRow = driver.findElements(By.xpath("//table[@id='customers-grid']//tbody/tr"));
   // By tableRows = By.xpath("//table[@id='customers-grid']//tbody/tr");


    public void sendEmail(String email)
    {
        searchEmail.clear();
        searchEmail.sendKeys(email);
    }

    public void sendFirstName(String fname)
    {
        searchFirstName.clear();
        searchFirstName.sendKeys(fname);
    }

    public void sendLastName(String lname)
    {
        searchLastName.clear();
        searchLastName.sendKeys(lname);
    }

    public int getListSize()
    {
        return tableRows.size();
    }

    public void clickSearchButton()
    {
        btnSearchCustomers.click();
    }

    public boolean findCustomerByEmail(String email){
        boolean flag = false;
        //List<WebElement> lstRows = driver.findElements(tableRows);
        for(int i=1;i<=tableRows.size();i++)
        {
            WebElement emailFromTable = table.findElement(By.xpath("//tbody/tr["+i+"]/td[2]"));
            if (email.equalsIgnoreCase(emailFromTable.getText()))
            {
                flag = true;
            }
        }
        return flag;
    }

    public boolean findCustomerByFirstName(String fname)
    {
        boolean flag = false;
        for (int i = 1;i<=tableRows.size();i++)
        {
            String nameFromTable = table.findElement(By.xpath("//tbody/tr["+i+"]/td[3]")).getText();
            String[] name = nameFromTable.split(" ");
            if (name[0].equalsIgnoreCase(fname))
            {
                flag = true;
            }
        }
        return flag;
    }
}
