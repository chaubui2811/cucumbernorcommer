package stepDefine;

/*import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;*/
import io.cucumber.java.Before;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import pageObject.AddNewCustomerPage;
import pageObject.LoginPage;
import pageObject.SearchCustomerPage;
import utilities.BaseClass;

import java.util.concurrent.TimeUnit;

public class Steps extends BaseClass {
    @Before
    public void setUp()
    {
        //logger
        BasicConfigurator.configure();
        logger = Logger.getLogger("nopComemrce");
        PropertyConfigurator.configure("src/test/java/log4j2.properties");

        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }
    @Given("^User Launch Chrome browser$")
    public void user_Launch_Chrome_browser() throws Throwable {
        lp = new LoginPage(driver);
    }

    @When("^User opens URL \"([^\"]*)\"$")
    public void user_opens_URL(String url) throws Throwable {
        //logger.info("******** Opening URL*********");
        driver.get(url);

    }

    @When("^User enters Email as \"([^\"]*)\" and Password as \"([^\"]*)\"$")
    public void user_enters_Email_as_and_Password_as(String username, String password) throws Throwable {
        logger.info("******** Providing login details*********");
        lp.setUserName(username);
        lp.setPassword(password);
    }

    @When("^Click on Login$")
    public void click_on_Login() throws InterruptedException {
        logger.info("******** started login*********");
        lp.clickLogin();
        Thread.sleep(3000);
    }

    @Then("^URL should be \"([^\"]*)\"$")
    public void URL_should_be(String url) throws Throwable {
/*        if (driver.getPageSource().contains("Login was unsuccessful."))
        {
            driver.close();
            logger.info("******** Login Failed*********");
            Assert.assertTrue(false);
        }else {
            logger.info("******** Login Passed*********");
            Assert.assertEquals(title, driver.getTitle());
        }
        Thread.sleep(3000);*/
        if (lp.isDisplayed(url)==false)
        {
            driver.close();
            logger.info("******** Login Failed*********");
            Assert.assertTrue(false);
        }else {
            logger.info("******** Login Passed*********");
        }
    }

    @When("^User click on Log out link$")
    public void user_click_on_Log_out_link() throws Throwable {
        logger.info("******** Click on logout link*********");
        lp.clickLogout();
        Thread.sleep(3000);
    }

    @Then("^close browser$")
    public void close_browser() throws Throwable {
        logger.info("********closing browser********");
        driver.quit();
    }

    @Then("User can view Dashboard")
    public void userCanViewDashboard() {
        ap = new AddNewCustomerPage(driver);
        Assert.assertEquals("Dashboard / nopCommerce administration",ap.getPageTitle());
    }

    @When("User click on customers Menu")
    public void userClickOnCustomersMenu() {
        ap.clickOnCustomerMenu();
    }

    @And("click on customers Menu Item")
    public void clickOnCustomersMenuItem() {
        ap.clickOnCustomerMenuItem();
    }

    @And("click on Add new button")
    public void clickOnAddNewButton() {
        ap.clickAddNewCustomer();
    }

    @Then("User can view Add new customer page")
    public void userCanViewAddNewCustomerPage() {
        Assert.assertEquals("Add a new customer / nopCommerce administration",ap.getPageTitle());
    }

    @When("User enter customer info")
    public void userEnterCustomerInfo() throws InterruptedException {
        logger.info("********Adding new customer*********");
        logger.info("********Proving customer details*********");
        String email = randomestring() +"@gmail.com";
        ap.setEmail(email);
        ap.setPassword("acb@123");
        ap.setCustomerRoles("Guest");
        Thread.sleep(3000);
        ap.setManagerOfVendor("Vendor 2");
        ap.setGender("Male");
        ap.setFirstName("NGO");
        ap.setLastName("TIEN");
        ap.setDob("8/12/1995");
        ap.setCompanyName("TkTUong");
        ap.setAdminContent("This is for testing.........");
    }

    @And("click on Save button")
    public void clickOnSaveButton() {
        ap.clickOnSave();
    }

    @Then("User can view confirmation message {string}")
    public void userCanViewConfirmationMessage(String arg0) {

    }

    @And("Enter customer EMail")
    public void enterCustomerEMail() {
        sp = new SearchCustomerPage(driver);
        sp.sendEmail("victoria_victoria@nopCommerce.com");
    }

    @When("Click on search button")
    public void clickOnSearchButton() {
        sp.clickSearchButton();
    }

    @Then("User should found Email in the Search table")
    public void userShouldFoundEmailInTheSearchTable() {
       // int numberSize = sp.getListSize();
      //  System.out.println("size of list is : " + numberSize);
        boolean searchResult = sp.findCustomerByEmail("victoria_victoria@nopCommerce.com");
        Assert.assertEquals(true,searchResult);
    }
}
